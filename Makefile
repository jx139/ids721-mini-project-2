.PHONY: build run test clean

build:
	cargo build

run:
	cargo run

test:
	cargo test

clean:
	cargo clean
