# Rust AWS Lambda Function with API Gateway Integration

[![pipeline status](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-2/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-2/-/commits/main)

This project contains a Rust-based AWS Lambda function designed to process data and respond to HTTP requests, integrated with AWS API Gateway.

## Prerequisites

- AWS account
- AWS CLI configured with necessary permissions
- Rust and Cargo installed
- Cargo Lambda installed
- Git installed

## Setup and Deployment

### Step 1: Configure AWS CLI

Ensure your AWS CLI is configured with the appropriate credentials.

```bash
aws configure
```

### Step 2: Install Cargo Lambda

If you haven't already, install Cargo Lambda using Cargo.

```bash
cargo install cargo-lambda
```

### Step 3: Create and Deploy the Lambda Function

#### Create a New Rust Lambda Project

```bash
cargo lambda new rust_lambda
```

#### Navigate to Project Directory

```bash
cd rust_lambda
```

#### Implement the Lambda Function

Modify `src/main.rs` with your function logic.

#### Build the Lambda Function

For `arm64` architecture:

```bash
cargo lambda build --release --arm64
```

#### Deploy the Lambda Function to AWS


```bash
cargo lambda deploy --iam-role 
```

### Step 4: Create IAM Role for Lambda

Ensure you have an IAM role with `AWSLambdaBasicExecutionRole` and any other necessary permissions.

### Step 5: Integrate with API Gateway

#### Create an HTTP API in AWS API Gateway

1. Navigate to API Gateway in the AWS Management Console.
2. Create a new HTTP API.
3. Set up an integration with your Lambda function.
4. Define the method (e.g., GET) and resource path (e.g., /process-data).
![api Page](./pic/api.png)

#### Deploy the API

1. Create a new stage (e.g., prod).
2. Deploy your API to make it accessible.

## Testing the Endpoint

Use the provided Invoke URL to test your API:

```plaintext
https://ba39atc9ja.execute-api.us-east-1.amazonaws.com/prod/process-data
```

![data Page](./pic/data.png)