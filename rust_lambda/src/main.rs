use lambda_runtime::{service_fn, Error, LambdaEvent};
use serde_json::{json, Value};

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn func(event: LambdaEvent<Value>) -> Result<Value, Error> {
    let (event, _context) = event.into_parts();
    println!("Processing event: {:?}", event);

    // Example of data processing: Transform the input JSON by adding a field
    let mut response = event;
    response["processed"] = json!("true");
    response["message"] = json!("Data processed successfully");

    Ok(response)
}
